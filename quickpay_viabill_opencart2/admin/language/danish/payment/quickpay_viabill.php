<?php
// Heading
$_['heading_title'] = 'ViaBill Quickpay Payment Option';

// Text 
$_['text_payment'] = 'Payment';
$_['text_success'] = 'Success: You have modified ViaBill Quickpay Checkout account details!';
$_['text_quickpay_viabill'] = '<a onclick="window.open(\'http://viabill.com/\');"><img  src="../catalog/view/viabillepay/viabill_logo.png" alt="ViaBill Quickpay" title="ViaBill Quickpay"  /></a>';

$_['text_authorization'] = 'Authorization';
$_['text_sale'] = 'Sale';

$_['text_edit'] = 'Rediger ' . $_['heading_title'];

// Entry
$_['entry_status'] = 'Status:';
$_['entry_merchant'] = 'Merchant ID:';
$_['entry_language'] = 'Language:';
$_['entry_apikey'] = 'API Key:';
$_['entry_autofee'] = 'Auto-gebyr:';
$_['entry_msgtype'] = 'Msgtype:';
$_['entry_autocapture'] = 'Autocapture:';
$_['entry_splitpayment'] = 'Splitpayment:';
//$_['entry_cardtypelock'] = 'Cardtypelock:';
$_['entry_order_status_completed'] = 'Ordre status ved gennemført ordre:';
$_['entry_secret'] = 'Secret Key:';
$_['entry_payment_methods'] = 'Betalings metoder:';
$_['entry_privatekey'] = 'Private key:';

// Tooltips
$_['tooltip_status'] = 'Aktiver / Deaktiver betalingsmetode';
$_['tooltip_merchant'] =  'Indtast dit Quickpay merchant ID';
$_['tooltip_agreement'] =  'Indtast dit Quickpay agreement ID';
$_['tooltip_apikey'] =  'Indtast din brugers API nøgle. Den kan findes under fanen <strong>Integration</strong> i jeres Quickpay manager.';
$_['tooltip_language'] = 'Vælg det viste sprog i betalingsvinduet';
$_['tooltip_msgtype'] = 'Vælg msgtype. Anbefalet: authorize';
$_['tooltip_autocapture'] = 'Aktiver automatisk hævning af betalinger.';
//$_['tooltip_cardtypelock'] = 'Angiv cardtypelock';
$_['tooltip_order_status_completed'] = 'Vælg ordrestatus på betalte ordrer';
$_['tooltip_autofee'] = 'Hvis aktiveret, vil et eventuelt gebyr fra indløser blive pålagt kundens transaktionsbeløb.';
$_['tooltip_privatekey'] = 'Indtast din brugers private nøgle. Den kan findes under fanen <strong>Integration</strong> i jeres Quickpay manager.';

// Error
$_['error_permission']   = 'Advarsel! Du har ikke tilladelse til at ændre QuickPay!';
$_['error_merchant'] = 'Merchant:';
$_['error_agreement'] = 'Agreement:';
$_['error_apikey'] = 'API nøgle:';
$_['error_continueurl'] = 'Continueurl:';
$_['error_cancelurl'] = 'Cancelurl:';
$_['error_callbackurl'] = 'Callbackurl:';
$_['error_secret'] = 'Secret:';
$_['error_msgtype'] = 'Msgtype:';
$_['error_cardtypelock'] = 'Cardtypelock:';
$_['error_privatekey'] = 'Private key:';

?>