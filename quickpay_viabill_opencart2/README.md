### Opencart2 : ViaBill QuickPay Module ###
----------------------  

ViaBill has developed a free payment module using QuickPay Payment Gateway which enables your customers to pay online for their orders in your Opencart 2 Web Shop.
ViaBill is a Payment Method and not a Payment Gateway.

###Facts###
---------
- version: 1.1
- Plugin on BitBucket (https://bitbucket.org/ibilldev/viabill-quickpay-opencart2-plugin/)


###Description###
-----------
Pay using ViaBill. 
Install this Plugin in to your Opencart2 Web Shop to provide a separate payment option to pay using ViaBill.

#Requirements
------------
* PHP >= 5.2.0

#Compatibility
-------------
* Opencart 2


###Integration Instructions###
-------------------------
1. Download the Module from the bitbucket. 

2. Module contains two folder a.) admin b.) catalog 

Please follow the following folder structure to place the relevant files for ViaBill QuickPay Integration:

Opencart2
 |
 +-- upload
 |  |  
 |  +-- admin
 |  |      |
 |	|	   |-- controller
 |  |      |		|-- payment
 |  |      |             |
 |  |      |             |
 |  |      |             +-- quickpay_viabill.php  
 |	|	   |
 |	|	   |-- view
 |	|	   |	|-- template
 |  |      |             |
 |  |      |             |-- payment
 |  |      |            		|
 |	|      |					|
 |  |      |					+-- quickpay_viabill.tpl
 |  |      |-- language
 |	|      |		|--english
 |  |	   |		|		|--payment
 |	|      |		|			|
 |	|      |        |       	+-- quickpay_viabill.php
 |	|      |        |
 |	|      |        |-- danish
 |	|      |        |		|--payment
 |	|      |        |			|
 |	|      |        |			+-- quickpay_viabill.php
 |	|      |        |
 |	|      |        |
 |  +-- catalog
 |	|	   |-- controller
 |  |      |		|-- payment
 |  |      |        |     |
 |  |      |        |     +-- quickpay_viabill.php  
 |  |      |		|-- checkout
 |  |      |             |
 |  |      |             +-- payment_method.php  
 |	|	   |
 |	|	   |-- model
 |	|	   |	|-- payment
 |  |      |           |
 |  |      |           +-- quickpay_viabill.php
 |	|      |					
 |	|	   |-- view
 |	|	   |	|-- theme
 |  |      |    |      |-- default
 |  |      |    |       	|-- template
 |	|      |	|				 |-- payment
 |	|      |	|				 |	|
 |	|      |	|			     |	+-- quickpay_viabill.tpl
 |	|      |	|				 |
 |	|      |	|				 |-- checkout
 |	|      |	|				 |		|
 |	|      |	|				 |		+payment_method.tpl
 |	|      |	|
 |	|      |	|-- quickpay_viabill
 |	|      |	|		|
 |	|      |	|		+--viabill_logo.png
 |	|      |	|
 |	|      |	|
 |  |      |-- language
 |	|      |		|--english
 |  |	   |		|		|--payment
 |	|      |		|			|
 |	|      |        |       	+-- quickpay_viabill.php
 |	|      |        |
 |	|      |        |
 |	|      |        |-- danish
 |	|      |        |		|--payment
 |	|      |        |			|
 |	|      |        |			+-- quickpay_viabill.php
 |	|	   |        |
 +

3. Copy the content of admin folder inside your project's admin folder (Follow the folder structure )

4. Copy the contents of catalog folder inside your project's cataog folder (Follow the folder structure )

5. Go to the Admin-> Extensions-> Payments. Look for "ViaBill Quickpay Payment Option"

6. Click on Install.

7. Now edit on the plugin.

8. Enable/Disable :  Please Enable the Status. 

9. Enter Merchant ID/Agreement ID/ API Key/ Private Key/Language

10. Save and Done.


##Uninstallation/Disable Module
-----------------------
1. Go to the Admin->Extensions->Payments
2. Look For "ViaBill Quickpay Payment Option"
3. Choose Option to Uninstall


#Support
-------
If you have any issues with this extension, kindly drop us a mail on [support@viabill.com](mailto:support@viabill.com)

#Contribution
------------


#License
-------
[OSL - Open Software Licence 3.0](http://opensource.org/licenses/osl-3.0.php)

#Copyright
---------
(c) 2015 ViaBill